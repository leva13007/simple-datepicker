# DatePicker

Install and config eslint

Add to package.json
```json
  "eslintConfig": {
    "extends": "react-app"
  },
  "devDependencies": {
    "eslint": "^6.6.0",
    "eslint-config-airbnb": "^18.2.0",
    "eslint-plugin-babel": "^5.3.1",
    "eslint-plugin-import": "^2.22.0",
    "eslint-plugin-jsx-a11y": "^6.3.1",
    "eslint-plugin-react": "^7.20.6",
    "eslint-plugin-react-hooks": "^4.1.1"
  }
```
After run 
```bash
npm i
```

```bash
cp .eslintrc.json.data .eslintrc.json 
```
