import React, { useEffect, useState } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import './style.css';

import { InputFieldComponent } from '../InputFieldComponent';
import { Calendar } from '../Calendar';

import { InputComponent } from './StyledCompnents/InputComponent';
import {DatePickerContextProvider} from "../../store/DatePickerContext";

const DatePickerWrapper = styled('div')`
  width: 100%;
`;

const LabelWrapper = styled('label')`
  margin: 8px 24px;
`;

const ErrorTextWrapper = styled('div')`
  margin: 8px 24px;
  color: #F40110;
`;

const DatePicker = ({
  placeholderText, selectedDate, setSelectedDate, dateInputFormat, beforeDay, afterDay, cellH, cellW,
}) => {
  const [displayedDate, setDisplayedDate] = useState('');
  const [errorInputDateStatus, setErrorInputDate] = useState(false);
  const [isShowCalendar, setShowCalendar] = useState(true);

  const onFocusHandler = () => {
    // setShowCursorInInput(true);
    setDisplayedDate(selectedDate ? displayedDate : '');
  };

  const onBlurHandler = () => {
    // setShowCursorInInput(false);
    // setDisplayedDate(selectedDate ? displayedDate : placeholderText); // todo show placeholder if need
  };

  const openCalendarHandler = () => {
    console.log('openCalendarHandler');
    setShowCalendar(true);
  };

  const sendToCalendarDate = () => ((!errorInputDateStatus && selectedDate) ? selectedDate : undefined);

  useEffect(() => {
    setErrorInputDate(false);

    if (displayedDate) {
      console.log('Valid', moment(displayedDate, dateInputFormat, true).isValid());
      // todo: add check date possible range
      if (!moment(displayedDate, dateInputFormat, true).isValid()) {
        setErrorInputDate(true);
      } else {
        setSelectedDate(displayedDate);
      }

      setDisplayedDate(displayedDate);
    } else {
      // setDisplayedDate(showCursorInInput ? '' : placeholderText);
    }
  }, [displayedDate]);

  return (
    <DatePickerContextProvider
      afterDay={afterDay}
      beforeDay={beforeDay}
      placeholderText={placeholderText}
      cellH={cellH}
      cellW={cellW}
    >
      <DatePickerWrapper>
        <LabelWrapper htmlFor="datepicker">День, місяць, рік.</LabelWrapper>
        <InputComponent hasErrorStatus={errorInputDateStatus}>
          <InputFieldComponent
            id="datepicker"
            value={displayedDate}
            changeValue={setDisplayedDate}
            openCalendar={openCalendarHandler}
            onFocus={onFocusHandler}
            onBlur={onBlurHandler}
            placeholderText={placeholderText}
          />
        </InputComponent>
        {
          errorInputDateStatus && (
            <ErrorTextWrapper>
              Error input date
            </ErrorTextWrapper>
          )
        }
        {isShowCalendar && (
          <Calendar
            selectDayHandler={(value) => { setSelectedDate(value); setDisplayedDate(value); }}
            selectedDate={sendToCalendarDate()}
            dateFormater={dateInputFormat}
            locale={'ua'}
            beforeDay={beforeDay}
            afterDay={afterDay}
          />
        )}
      </DatePickerWrapper>
    </DatePickerContextProvider>
  );
};

DatePicker.propTypes = {
  placeholderText: PropTypes.string,
  beforeDay: PropTypes.instanceOf(moment),
  afterDay: PropTypes.instanceOf(moment),
  cellH: PropTypes.number,
  cellW: PropTypes.number,
};

DatePicker.defaultProps = {
  placeholderText: 'Enter date, for example 01092020',
  beforeDay: moment().startOf('month'),
  afterDay: moment().endOf('month'),
  cellH: 24,
  cellW: 24,
};

export { DatePicker };
