import React from 'react';
import styled from 'styled-components';

const InputWrapper = styled('div')`
  position:relative;
  margin: 8px 24px;
  height: 48px;
  border: 1px solid ${(props) => (props.hasErrorStatus ? '#F40110' : '#E1E0E0')};
  border-radius: 10px;
  display: flex;
  align-items: center;
  //padding: 16px;
  overflow: hidden;
  color: ${(props) => (props.hasErrorStatus ? '#F40110' : '#333333')};
`;

const InputComponent = ({ children, hasErrorStatus }) => (
  <InputWrapper hasErrorStatus={hasErrorStatus}>
    {children}
  </InputWrapper>
);

export { InputComponent };
