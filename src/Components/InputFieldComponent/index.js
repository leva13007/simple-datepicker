import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import calendar from '../../asset/img/calendar.svg';

const InputWrapper = styled('div')`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  
  & input{
    //opacity: 0;
    padding: 16px;
    border: unset;
    height: 48px;
    width: 100%;
      &:focus{
      outline: none;
     }
  }
`;

const ButtonWrapper = styled('button')`
  border: unset;
  background: unset;
  padding: 0;
  display: flex;
  position: absolute;
  right: 16px;
  top: 50%;
  transform: translate(0, -50%);
  cursor:pointer;
  outline: none;
`;

const InputFieldComponent = ({
  id, value, changeValue, openCalendar, onFocus, onBlur, placeholderText,
}) => (
  <InputWrapper>
    <input
      id={id}
      type="text"
      value={value}
      onChange={(e) => changeValue(e.target.value)}
      onFocus={onFocus}
      onBlur={onBlur}
      placeholder={placeholderText}
    />
    <ButtonWrapper type="button" onClick={openCalendar}>
      <img src={calendar} alt="" />
    </ButtonWrapper>
  </InputWrapper>
);

InputFieldComponent.propTypes = {
  id: PropTypes.string.isRequired,
  value: PropTypes.string,
  changeValue: PropTypes.func.isRequired,
  openCalendar: PropTypes.func.isRequired,
  onFocus: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
};

InputFieldComponent.defaultProps = {
  value: null,
};

export { InputFieldComponent };
