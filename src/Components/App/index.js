import React, { useState } from 'react';
import styled from 'styled-components';
import moment from 'moment';

import { DatePicker } from '../DatePicker';

const AppWrapper = styled('div')`
  width: 400px;
  height: 224px;
  background: #FFFFFF;
  box-shadow: 0px 5px 15px rgba(0, 0, 0, 0.15);
  border-radius: 10px;
  //position: absolute;
  //left: 50%;
  //top: 50%;
  //transform: translate(-50%, -50%);
  display: flex;
  align-items: center;
`;

const App = () => {
  const [selectedDate, setSelectedDate] = useState('');
  return (
    <AppWrapper>
      <DatePicker
        selectedDate={selectedDate}
        setSelectedDate={setSelectedDate}
        dateInputFormat="DD/MM/YYYY"
        beforeDay={moment().subtract(35, 'year').startOf('month')}
        afterDay={moment().add(10, 'year').endOf('month')}
      />
    </AppWrapper>
  );
};

export { App };
