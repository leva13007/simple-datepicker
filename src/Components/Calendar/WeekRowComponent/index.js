import React from 'react';
import moment from 'moment';

import { WeekDayWrapper } from '../StyledCompnents/WeekDayWrapper';
import { WeekWrapper } from '../StyledCompnents/WeekWrapper';

const WeekRowComponent = () => {
  const weekDays = moment.weekdaysMin();

  return (
    <WeekWrapper>
      {
        weekDays.map((_) => <WeekDayWrapper key={_}>{_}</WeekDayWrapper>)
      }
    </WeekWrapper>
  );
};

export { WeekRowComponent };
