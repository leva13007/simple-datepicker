import React from 'react';
import styled from 'styled-components';

const CalendarBlockWrapper = styled('div')`
  background: #FFFFFF;
  box-shadow: 0px 5px 15px rgba(0, 0, 0, 0.15);
  border-radius: 12px;
  display: flex;
  flex-direction: column;
  padding: 16px;
`;
const CalendarBlockComponent = ({children}) => (
  <CalendarBlockWrapper>
    {children}
  </CalendarBlockWrapper>
);

export { CalendarBlockComponent };
