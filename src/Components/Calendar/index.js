import React from 'react';
import moment from 'moment';

import { useDatePickerContext } from '../../store/DatePickerContext';

import { OverlayComponent } from './OverlayComponent';
import { CalendarBlockComponent } from './CalendarBlockComponent';
import { DayWrapper } from './StyledCompnents/DayWrapper';
import { WeekRowComponent } from './WeekRowComponent';
import { WeekWrapper } from './StyledCompnents/WeekWrapper';
import { SelectControlComponent } from './SelectControlComponent';

const Calendar = ({
 selectedDate, selectDayHandler, dateFormater, locale,
}) => {
  moment.updateLocale('ua', {
    week: {
      dow: 1, // First day of week is Monday
    },
    weekdays: [
      'Sunday', 'Monday', 'Tuesday', 'Середа', 'Thursday', 'Friday', 'Saturday',
    ],
    weekdaysShort: ['Sun', 'Mon', 'Tue', 'Сер', 'Thu', 'Fri', 'Sat'],
    weekdaysMin: ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Нд'],
    months: [
      'January', 'February', 'March', 'April', 'May', 'June', 'July',
      'August', 'September', 'October', 'November', 'December',
    ],
    monthsShort: [
      'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec',
    ],
  });

  const momentInstance = moment(selectedDate, selectedDate && dateFormater).locale(locale);

  const startDay = momentInstance.clone().startOf('month').startOf('week');
  const endDay = momentInstance.clone().endOf('month').endOf('week');

  const calendar = [];
  const day = startDay.clone().subtract(1, 'day'); // start offset on one day!

  while (day.isBefore(endDay, 'day')) {
    calendar.push(
      [...Array(7)]
        .map(() => day.add(1, 'day').clone()),
    );
  }

  const { beforeDay, afterDay } = useDatePickerContext();
  return (
    <OverlayComponent>
      <CalendarBlockComponent>
        <SelectControlComponent momentInstance={momentInstance} selectDayHandler={selectDayHandler} dateFormater={dateFormater} />
        <WeekRowComponent />
        <div>
          {
            calendar.map((week) => (
              <WeekWrapper key={Symbol(week).toString()}>
                {
                  week.map((_) => (
                    <DayWrapper
                      onClick={() => _.isSameOrBefore(afterDay) && _.isSameOrAfter(beforeDay) && selectDayHandler(_.format(dateFormater))}
                      selected={_.isSame(momentInstance)}
                      disabled={!(_.isSameOrBefore(afterDay) && _.isSameOrAfter(beforeDay))}
                      key={_}
                      isInCurrentMonth={_.isSame(momentInstance, 'month')}
                    >
                      {_.format('D').toString()}
                    </DayWrapper>
                  ))
                }
              </WeekWrapper>
            ))
          }
        </div>
      </CalendarBlockComponent>
    </OverlayComponent>
  );
};

export { Calendar };
