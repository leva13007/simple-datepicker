import React from 'react';
import styled from 'styled-components';

const OverlayWrapper = styled('div')`
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: rgba(255, 255, 255, 0.75);
    display: flex;
    align-items: center;
`;

const OverlayComponent = ({children}) => (
  <OverlayWrapper>
    {children}
  </OverlayWrapper>
);

export { OverlayComponent };
