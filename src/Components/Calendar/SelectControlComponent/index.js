import React from 'react';
import range from 'lodash/range';
import styled from 'styled-components';

import arrowBack from '../../../asset/img/arrow_back.svg';
import arrowForward from '../../../asset/img/arrow_forward.svg';
import caretDown from '../../../asset/img/caret_down.svg';
import { useDatePickerContext } from '../../../store/DatePickerContext';

const FlexWrapper = styled('div')`
  display: flex;
  align-items: center;
`;

const MonthContainerWrapper = styled(FlexWrapper)`
  flex: 1;
`;

const ButtonWrapper = styled('button')`
  border: unset;
  background: unset;
  padding: 0;
  display: flex;
  outline: none;
  cursor: ${(props) => props.disabled ? 'not-allowed' : 'pointer'};
`;

const MonthWrapper = styled('div')`
  min-width: 96px;
`;

const YearItemWrapper = styled(ButtonWrapper)`
  width: 52px;
  height: 24px;
  align-items: center;
  justify-content: center;
  background: ${(props) => (props.selected ? '#333333' : 'unset')};
  color: ${(props) => (props.selected ? '#fff' : 'unset')};
  border-radius: 24px;
`;

const YearsRangeWrapper = styled('div')`
  width: ${(props) => ((props.cellW + 1) * 7 + 7 * 8)}px;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`;

const SelectControlComponent = ({momentInstance, selectDayHandler, dateFormater}) => {
  const { beforeDay, afterDay, cellW } = useDatePickerContext();
  console.log(momentInstance.format('MMMM'))

  const getMonth = () => momentInstance.format('MMMM');
  const getYear = () => momentInstance.format('YYYY');

  const yearsRange = [...range(beforeDay.format('YYYY'), afterDay.format('YYYY')), +afterDay.format('YYYY')];
  console.log('duration',afterDay.format('YYYY'), beforeDay.format('YYYY'), yearsRange);

  return (
    <>
      <FlexWrapper>
        <MonthContainerWrapper>
          <MonthWrapper>{getMonth()}</MonthWrapper>
          <FlexWrapper>
            <ButtonWrapper
              onClick={() => selectDayHandler(momentInstance.subtract(1, 'month').format(dateFormater))}
              disabled={!momentInstance.clone().subtract(1, 'month').isSameOrAfter(beforeDay)}
            >
              <img src={arrowBack} alt="" />
            </ButtonWrapper>
            <ButtonWrapper
              onClick={() => selectDayHandler(momentInstance.add(1, 'month').format(dateFormater))}
              disabled={!momentInstance.clone().add(1, 'month').isSameOrBefore(afterDay)}
            >
              <img src={arrowForward} alt="" />
            </ButtonWrapper>
          </FlexWrapper>
        </MonthContainerWrapper>
        <FlexWrapper>{getYear()}<ButtonWrapper><img src={caretDown} alt="" /></ButtonWrapper></FlexWrapper>
      </FlexWrapper>
      <YearsRangeWrapper cellW={cellW}>
        {
          yearsRange.map(_ => (
            <YearItemWrapper
              onClick={() => selectDayHandler(momentInstance.clone().year(_).format(dateFormater))}
              selected={momentInstance.clone().year(_).isSame(momentInstance, 'year')}
            >
              {_}
            </YearItemWrapper>
          ))
        }
      </YearsRangeWrapper>
    </>
  );
};

export { SelectControlComponent };
