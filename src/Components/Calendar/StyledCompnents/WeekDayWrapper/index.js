import styled from 'styled-components';

const WeekDayWrapper = styled('div')`
  font-size: 14px;
  width: 25px;
  height: 25px;
  margin: 4px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export { WeekDayWrapper };
