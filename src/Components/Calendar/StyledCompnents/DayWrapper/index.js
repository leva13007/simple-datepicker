import styled from 'styled-components';

import { WeekDayWrapper } from '../WeekDayWrapper';

const DayWrapper = styled(WeekDayWrapper)`
  cursor: ${(props) => props.disabled ? 'not-allowed' : 'pointer'}; 
  &:hover{
    background: ${(props) => props.disabled ? 'unset' : '#E1E0E0'};
  }
  
  background: ${(props) => (props.selected ? '#333333' : 'unset')};
  color: ${(props) => (props.selected ? '#fff' : props.isInCurrentMonth ? '#333333' : '#ddd')};
  border-radius: 50%;
`;

export { DayWrapper };
