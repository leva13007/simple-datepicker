import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

const DatePickerContext = React.createContext();
DatePickerContext.displayName = 'DatePickerContext';

const useDatePickerContext = () => useContext(DatePickerContext);

const DatePickerContextProvider = ({
  children,
  placeholderText,
  beforeDay,
  afterDay,
  cellH,
  cellW,
}) => (
  <DatePickerContext.Provider value={{
    placeholderText,
    beforeDay,
    afterDay,
    cellH,
    cellW,
  }}
  >
    {children}
  </DatePickerContext.Provider>
);

DatePickerContext.propTypes = {
  children: PropTypes.node.isRequired,
  placeholderText: PropTypes.string.isRequired,
  beforeDay: PropTypes.instanceOf(moment),
  afterDay: PropTypes.instanceOf(moment),
  cellH: PropTypes.number.isRequired,
  cellW: PropTypes.number.isRequired,
};

export { DatePickerContextProvider, useDatePickerContext };
